package com.geekhub.web;

import com.geekhub.calculator.figure.Circle;
import com.geekhub.calculator.figure.Rectangle;
import com.geekhub.calculator.figure.Square;
import com.geekhub.calculator.figure.Triangle;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @RequestMapping(value = "/circle", method = RequestMethod.GET)
    public String circleArea(Model model){
        model.addAttribute("circle", new Circle());
        return "circle";
    }

    @RequestMapping(value = "/circle", method = RequestMethod.POST)
    public String circleArea(@ModelAttribute("circle") Circle circle, BindingResult result){
        if (result.hasErrors()){
            return "circle";
        }
        Circle circle1 = new Circle();
        circle1.setRadius(circle.getRadius());
        String area = String.format("%.2f", circle1.calculateArea());
        String circleLength = String.format("%.2f", circle1.calculatePerimeter());

        return "redirect:/circleResult?area="+area+"&area="+circleLength;
    }

    @RequestMapping(value = "/rectangle", method = RequestMethod.GET)
    public String rectangleArea(Model model){
        model.addAttribute("rectangle", new Rectangle());
        return "rectangle";
    }

    @RequestMapping(value = "/rectangle", method = RequestMethod.POST)
    public String rectangleArea(@ModelAttribute("rectangle") Rectangle rectangle, BindingResult result){
        if (result.hasErrors()){
            return "rectangle";
        }
        Rectangle rectangle1 = new Rectangle();
        rectangle1.setLength(rectangle.getLength());
        rectangle1.setWidth(rectangle.getWidth());
        String area = String.format("%.2f", rectangle1.calculateArea());
        String perimeter = String.format("%.2f", rectangle1.calculatePerimeter());

        return "redirect:/rectangleResult?area="+area+"&area="+perimeter;
    }

    @RequestMapping(value = "/square", method = RequestMethod.GET)
    public String squareArea(Model model){
        model.addAttribute("square", new Square());
        return "square";
    }

    @RequestMapping(value = "/square", method = RequestMethod.POST)
    public String squareArea(@ModelAttribute("square") Square square, BindingResult result){
        if (result.hasErrors()){
            return "square";
        }
        Square square1 = new Square();
        square1.setLengthSide(square.getLengthSide());
        String area = String.format("%.2f", square1.calculateArea());
        String perimeter = String.format("%.2f", square1.calculatePerimeter());

        return "redirect:/squareResult?area="+area+"&area="+perimeter;
    }

    @RequestMapping(value = "/triangle", method = RequestMethod.GET)
    public String triangleArea(Model model){
        model.addAttribute("triangle", new Triangle());
        return "triangle";
    }

    @RequestMapping(value = "/triangle", method = RequestMethod.POST)
    public String triangleArea(@ModelAttribute("triangle") Triangle triangle, BindingResult result){
        if (result.hasErrors()){
            return "triangle";
        }
        Triangle triangle1 = new Triangle();
        triangle1.setSideA(triangle.getSideA());
        triangle1.setSideB(triangle.getSideB());
        triangle1.setSideC(triangle.getSideB());
        String area = String.format("%.2f", triangle1.calculateArea());
        String perimeter = String.format("%.2f", triangle1.calculatePerimeter());
        return "redirect:/triangleResult?area="+area+"&area="+perimeter;
    }

    @RequestMapping(value = "/circleResult", method = RequestMethod.GET)
    public String circleResult(){
        return "circleResult";
    }

    @RequestMapping(value = "/rectangleResult", method = RequestMethod.GET)
    public String rectangleResult(){
        return "rectangleResult";
    }

    @RequestMapping(value = "/squareResult", method = RequestMethod.GET)
    public String squareResult(){
        return "squareResult";
    }

    @RequestMapping(value = "/triangleResult", method = RequestMethod.GET)
    public String triangleResult(){
        return "triangleResult";
    }
}
