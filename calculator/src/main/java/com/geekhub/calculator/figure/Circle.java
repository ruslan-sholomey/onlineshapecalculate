package com.geekhub.calculator.figure;

import com.geekhub.calculator.iShape.IShape;

public class Circle implements IShape {

    private int radius;
    private double area;
    private double perimeter;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public double calculateArea() {
        area = Math.PI * Math.pow(getRadius(), 2);
        return area;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = (2 * Math.PI) * getRadius();
        return perimeter;
    }
}
