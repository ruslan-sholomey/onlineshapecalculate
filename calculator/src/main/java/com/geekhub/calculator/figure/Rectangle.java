package com.geekhub.calculator.figure;

import com.geekhub.calculator.iShape.IShape;

public class Rectangle implements IShape {

    public Rectangle(){}

    private int length;
    private int width;
    private double area;
    private double perimeter;

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public double calculateArea() {
        area = getLength() * getWidth();
        return area;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = (getLength() * 2) + (getWidth() * 2);
        return perimeter;
    }
}
