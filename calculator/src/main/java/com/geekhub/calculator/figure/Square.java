package com.geekhub.calculator.figure;

import com.geekhub.calculator.iShape.IShape;

public class Square implements IShape {

    public Square(){}

    private double lengthSide;
    private double area;
    private double perimeter;

    public double getLengthSide() {
        return lengthSide;
    }

    public void setLengthSide(double lengthSide) {
        this.lengthSide = lengthSide;
    }

    @Override
    public double calculateArea() {
        area = Math.pow(getLengthSide(), 2);
        return area;
    }

    @Override
    public double calculatePerimeter() {
        perimeter = getLengthSide() * 4;
        return perimeter;
    }
}

