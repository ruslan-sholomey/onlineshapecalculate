package com.geekhub.calculator.iShape;

public interface IShape {

    double calculateArea();
    double calculatePerimeter();
}
